#!/usr/bin/python

try:
    from rq import Worker, Connection
    from redis import Redis
    from rq import Queue
    from my_module import download, func, conv_to_tif, calc_index
except ImportError:
    print 'Import is failed'

listen = ['high', 'medium', 'low']

conn = Redis(port=6379)
q = Queue('medium', connection=conn)

q.enqueue(func, 2, 3)  #test
q.enqueue(download,
          'http://e4ftl01.cr.usgs.gov/MOLT/',
          'MOD09GQ.005',
          '2000.08.26',
          'MOD09GQ.A2000239.h17v04.005.2006300122924.hdf',
          timeout=240)
q.enqueue(conv_to_tif, 'sttr')
q.enqueue(calc_index, 'data')

print 'name: ' + q.name + '; count: ' + str(q.count)

with Connection(conn):
    worker = Worker(map(Queue, listen))
    worker.work()

