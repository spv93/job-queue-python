#!/usr/bin/python

def func(x, y):
    return x * y


def download(site, product, date, name):
    '''
    name = 'MOD09GQ.A2000239.h17v04.005.2006300122924.hdf'
    date = '2000.08.26'
    product = 'MOD09GQ.005'
    site = 'http://e4ftl01.cr.usgs.gov/MOLT/'
    '''

    import urllib

    url = site + '/' + product + '/' + date + '/' + name

    urllib.urlretrieve(url, 'downloads/' + name)

    return True


def conv_to_tif(sttr):
    '''
    ..............
    '''
    return 'Successful conv to tif'


def calc_index(data):
    '''
    ..............
    '''
    return 'Successful calc index'




